﻿using System;

/// <summary>
/// Summary description for Class1
/// </summary>

namespace WeatherAppDemo
{

    public class LocationData
    {
        public async static Task<Geoposition> getPosition()
        {
            var accessStatus = await Geolocator.RequestAccessAsync();
            if (accessStatus != GeolocationAccessStatus.Allowed) throw new Exception();
            var geolocator = new Geolocator { DesiredAccuracyInMeters = 0 };
            var postion = await geolocator.GetGeoppositionAsync();
            return postion;
            //
            // TODO: Add constructor logic here
            //
        }
    }
}